//
//  UITapGestureExtensions.swift
//  Duoyulong
//
//  Created by Aleksandr Solovyev on 29.01.2018.
//  Copyright © 2018 Duoyulong. All rights reserved.
//

import Foundation

public protocol TapGestureWithClosureProtocol {
   var tapGestureWithClosure: TapGestureRecognizerWithClosure! {get set}
}

public class TapGestureRecognizerWithClosure: NSObject {
   
   public var closure: () -> Void
   
   public init(view: UIView, closure: @escaping () -> Void) {
      
      let recognizer: UITapGestureRecognizer = UITapGestureRecognizer()
      self.closure = closure
      
      super.init()
      
      view.addGestureRecognizer(recognizer)
      recognizer.addTarget(self, action: #selector(invokeTarget(recognizer:)))
   }
   
   @objc func invokeTarget(recognizer: UIGestureRecognizer) {
      self.closure()
   }
}
