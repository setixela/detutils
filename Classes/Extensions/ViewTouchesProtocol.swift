//
//  ViewTouchesProtocol.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 21.02.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

extension UIView {
   ////////
   override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      super.touchesBegan(touches, with: event)
      
      if let slf = self as? ViewTouchesProtocol {
         if let location = touches.first?.location(in: self) {
            slf.touchesBegan(x: location.x, y: location.y)
         }
      }
   }
   
   override open func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
      super.touchesMoved(touches, with: event)
      
      if let slf = self as? ViewTouchesProtocol {
         if let location = touches.first?.location(in: self) {
            slf.touchesMoved(x: location.x, y: location.y)
         }
      }
   }
   
   override open func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
      super.touchesCancelled(touches, with: event)
      
      if let slf = self as? ViewTouchesProtocol {
         if let location = touches.first?.location(in: self) {
            slf.touchesCancelled(x: location.x, y: location.y)
         }
      }
   }
   
   override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
      super.touchesEnded(touches, with: event)
      
      if let slf = self as? ViewTouchesProtocol {
         if let location = touches.first?.location(in: self) {
            slf.touchesEnded(x: location.x, y: location.y)
         }
      }
   }
   
   //////////////////////
}

public protocol ViewTouchesProtocol {
   func touchesBegan(x: CGFloat, y: CGFloat)
   func touchesMoved(x: CGFloat, y: CGFloat)
   func touchesCancelled(x: CGFloat, y: CGFloat)
   func touchesEnded(x: CGFloat, y: CGFloat)
}
