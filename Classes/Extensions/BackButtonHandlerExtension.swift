//
//  BackButtonHandlerExtension.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 13.09.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public protocol BackButtonHandlerProtocol {
   func shouldPopOnBackButton() -> Bool
}

extension UINavigationController: UINavigationBarDelegate {
   public func navigationBar(_ navigationBar: UINavigationBar, shouldPop item: UINavigationItem) -> Bool {
      if viewControllers.count < (navigationBar.items?.count) ?? 0 {
         return true
      }
      
      var shouldPop = true
      let vc = topViewController
      
      if let vc = vc as? BackButtonHandlerProtocol {
         shouldPop = vc.shouldPopOnBackButton()
      }
      
      if shouldPop {
         DispatchQueue.main.async { [weak self] in
            _ = self?.popViewController(animated: true)
         }
      } else {
         for subView in navigationBar.subviews {
            if 0 < subView.alpha && subView.alpha < 1 {
               UIView.animate(withDuration: 0.25) {
                  subView.alpha = 1
               }
            }
         }
      }
      
      return false
   }
}
