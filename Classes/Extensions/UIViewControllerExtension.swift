//
//  UIViewControllerExtension.swift
//  SetixelaUtils
//
//  Created by Aleksandr Solovyev on 04.06.2018.
//  Copyright © 2018 XYZ Project. All rights reserved.
//

import Foundation

public extension UIViewController {
   public func removeNaveBarShadows() {
      navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
      navigationController?.navigationBar.shadowImage = UIImage()
   }
}
