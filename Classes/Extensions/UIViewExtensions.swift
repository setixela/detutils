//
//  UIViewExtensions.swift
//  GdeDuet2
//
//  Created by Setixela on 11.07.16.
//  Copyright © 2016 XYZ Project. All rights reserved.
//

import Foundation
import UIKit

@objc public extension UIView {

   // MARK: -------------------------------------------------- load from nib
   
   // MARK: -------------------------------------------------- load from nib
   
   public convenience init(toView: UIView) {
      self.init()
      
      toView.addSubview(self)
   }
   
   public class func loadFromNib(_ name: String, bundle: Bundle? = nil) -> AnyObject {
      return (UINib(nibName: name, bundle: bundle).instantiate(withOwner: nil, options: nil)[0] as? UIView)!
   }
   
   public class func loadToView(_ view: UIView, name: String) -> AnyObject {
      // let classType = type(of: self)
      // let className: String = String(describing: classType)
      if let newView = UIView.loadFromNib(name) as? UIView {
         view.addSubview(newView)
         return newView
      } else {
         return UIView()
      }
   }
   
   // MARK: -------------------------------------------------- left right top bottom
   
   public var x: CGFloat {
      get {
         return (frame.origin.x)
      }
      set {
         frame.origin.x = newValue
      }
   }
   
   public var y: CGFloat {
      get {
         return (frame.origin.y)
      }
      set {
         frame.origin.y = newValue
      }
   }
   
   public var bottom: CGFloat {
      get {
         return (frame.origin.y + frame.size.height)
      }
      set {
         frame.origin.y = newValue - frame.size.height
      }
   }
   
   public var top: CGFloat {
      get {
         return (frame.origin.y)
      }
      set {
         frame.origin.y = newValue
      }
   }
   
   public var left: CGFloat {
      get {
         return (frame.origin.x)
      }
      set {
         frame.origin.x = newValue
      }
   }
   
   public var right: CGFloat {
      get {
         return (frame.origin.x + frame.size.width)
      }
      set {
         frame.origin.x = newValue - frame.size.width
      }
   }
   
   // MARK: -------------------------------------------------- width height
   
   public var width: CGFloat {
      get {
         return frame.size.width
      }
      set {
         frame.size.width = newValue
      }
   }
   
   public var height: CGFloat {
      get {
         return frame.size.height
      }
      set {
         frame.size.height = newValue
      }
   }
   
   public var halfWidth: CGFloat {
      return frame.size.width / 2
   }
   
   public var halfHeight: CGFloat {
      return frame.size.height / 2
   }
   
   // MARK: -------------------------------------------------- Bounds
   
   public var boundsWidth: CGFloat {
      return self.bounds.size.width
   }
   
   public var boundsHeight: CGFloat {
      return self.bounds.size.height
   }
   
   // MARK: -------------------------------------------------- set frame origins
   
   public func scaleRightToX(x: CGFloat) {
      let prevLeft = self.left
      let xDif = right - x
      self.width -= xDif
      self.left = prevLeft
   }
   
   public func scaleLeftToX(x: CGFloat) {
      let prevRight = self.right
      let xDif = left - x
      self.width += xDif
      self.right = prevRight
   }
   
   public func scaleTopToY(y: CGFloat) {
      let prevBottom = self.bottom
      let yDif = top - y
      self.height += yDif
      self.bottom = prevBottom
   }
   
   public func scaleBottomToY(y: CGFloat) {
      let prevTop = self.top
      let yDif = bottom - y
      self.height -= yDif
      self.top = prevTop
   }
}

@objc public extension UIView {
   public func setSizeAtCenter(size: CGSize) {
      let center = self.center
      self.frame.size = size
      self.center = center
   }
   
   public func set(top: CGFloat, centerX: CGFloat, width: CGFloat, height: CGFloat) {
      self.frame.size = CGSize(width: width, height: height)
      self.top = top
      self.center.x = centerX
   }
   
   public func set(centerX: CGFloat, centerY: CGFloat, width: CGFloat, height: CGFloat) {
      self.frame.size = CGSize(width: width, height: height)
      self.center.x = centerX
      self.center.y = centerY
   }
   
   public func set(left: CGFloat, centerY: CGFloat, width: CGFloat, height: CGFloat) {
      self.frame.size = CGSize(width: width, height: height)
      self.x = left
      self.center.y = centerY
   }
   
   public func set(right: CGFloat, centerY: CGFloat, width: CGFloat, height: CGFloat) {
      self.frame.size = CGSize(width: width, height: height)
      self.right = right
      self.center.y = centerY
   }
   
   public func set(right: CGFloat, top: CGFloat, width: CGFloat, height: CGFloat) {
      self.frame.size = CGSize(width: width, height: height)
      self.right = right
      self.top = top
   }
   
   public func set(left: CGFloat, top: CGFloat, width: CGFloat, height: CGFloat) {
      self.frame.size = CGSize(width: width, height: height)
      self.left = left
      self.top = top
   }
   
   public func set(centerX: CGFloat, bottom: CGFloat, width: CGFloat, height: CGFloat) {
      self.frame.size = CGSize(width: width, height: height)
      self.center.x = centerX
      self.bottom = bottom
   }
   
   public func set(centerX: CGFloat, top: CGFloat, width: CGFloat, height: CGFloat) {
      self.frame.size = CGSize(width: width, height: height)
      self.center.x = centerX
      self.top = top
   }
   
   public func set(right: CGFloat, bottom: CGFloat, width: CGFloat, height: CGFloat) {
      self.frame.size = CGSize(width: width, height: height)
      self.right = right
      self.bottom = bottom
   }
   
   public func set(bottom: CGFloat, centerX: CGFloat, width: CGFloat, height: CGFloat) {
      self.frame.size = CGSize(width: width, height: height)
      self.center.x = centerX
      self.bottom = bottom
   }
   
   ///
   public var centerX: CGFloat {
      set {
         self.center.x = newValue
      }
      
      get {
         return self.center.x
      }
   }
   
   public var centerY: CGFloat {
      set {
         self.center.y = newValue
      }
      
      get {
         return self.center.y
      }
   }
}

public extension UIView {
   public func rotate360Degrees(duration: CFTimeInterval = 3) {
      let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
      rotateAnimation.fromValue = 0.0
      rotateAnimation.toValue = CGFloat(CGFloat.pi * 2)
      rotateAnimation.isRemovedOnCompletion = false
      rotateAnimation.duration = duration
      rotateAnimation.repeatCount = Float.infinity
      self.layer.add(rotateAnimation, forKey: nil)
   }
}

public extension UIView {
   public func parentView<T: UIView>(of type: T.Type) -> T? {
      guard let view = self.superview else {
         return nil
      }
      return (view as? T) ?? view.parentView(of: T.self)
   }
}

// undocumented method!
// extension UITableViewCell {
//   var tableView: UITableView? {
//      return self.parentView(of: UITableView.self)
//   }
// }

// MARK: -------------------------------------------------------- animations

public extension UIView {
   public func spinWith(duration: CGFloat, rotations: CGFloat, repeatCount: Float) {
      let rotationAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
      
      rotationAnimation.toValue = CGFloat.pi * 2 * rotations
      rotationAnimation.duration = CFTimeInterval(duration)
      rotationAnimation.isCumulative = true
      rotationAnimation.repeatCount = Float(repeatCount)
      self.layer.add(rotationAnimation, forKey: "rotationAnimation")
   }
}

// MARK: -------------------------------------------------------- corners radius

public extension UIView {
   public func roundCorners(corners: UIRectCorner, radius: CGFloat) {
      let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
      let mask = CAShapeLayer()
      mask.path = path.cgPath
      self.layer.mask = mask
   }
   
   ///////////////////
   public func addExternalBorder(width: CGFloat) {
      guard self.layer.sublayers?.first == nil else { return }
      
      let externalBorder = CALayer()
      externalBorder.frame = CGRect(x: -width, y: -width, width: self.frame.width + width * 2, height: self.frame.height + width * 2)
      externalBorder.cornerRadius = self.layer.cornerRadius
      externalBorder.borderWidth = width
      externalBorder.borderColor = UIColor.black.cgColor
      externalBorder.backgroundColor = UIColor.clear.cgColor
      self.layer.addSublayer(externalBorder)
      
      self.layer.masksToBounds = false
   }
   
   public func setExternalBorder(color: UIColor) {
      if let externalBorder = self.layer.sublayers?.first {
         externalBorder.borderColor = color.cgColor
      }
   }
   
   public func layoutExternalBorder() {
      if let externalBorder = self.layer.sublayers?.first {
         let width = externalBorder.borderWidth
         externalBorder.frame = CGRect(x: -width, y: -width, width: self.frame.width + width * 2, height: self.frame.height + width * 2)
      }
   }
}

public extension UIView {
   public enum BaseDesignScreenType {
      case
         iPhone5,
         iPhone6,
         iPhonePlus,
         iPhoneX
   }
   
   public func setFrame(x: CGFloat? = nil, y: CGFloat? = nil, width: CGFloat? = nil, height: CGFloat? = nil, screenType: BaseDesignScreenType) {
      var x = x
      var y = y
      var width = width
      var height = height
      
      var screenCoef: CGFloat = 1
      switch screenType {
      case .iPhone5:
         screenCoef = UIScreen.main.bounds.size.width / 320.0
      case .iPhone6:
         screenCoef = UIScreen.main.bounds.size.width / 375.0
      default:
         break
      }
      
      if x == nil {
         x = self.frame.origin.x
      }
      if y == nil {
         y = self.frame.origin.y
      }
      if width == nil {
         width = self.frame.size.width
      }
      if height == nil {
         height = self.frame.size.height
      }
      
      self.frame = CGRect(x: x! * screenCoef, y: y! * screenCoef, width: width! * screenCoef, height: height! * screenCoef)
   }
}

public extension UIView {
   static var flexible: UIView.AutoresizingMask {
      return [.flexibleWidth, .flexibleHeight]
   }
}
