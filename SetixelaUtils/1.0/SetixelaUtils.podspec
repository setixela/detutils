#
#  Be sure to run `pod spec lint SetixelaUtils.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "SetixelaUtils"
  s.version      = "1.0"
  s.summary      = "Setixela Swift Utils."
  s.description  = ""
  s.license      = "MIT"
  s.author       = { "Aleksandr Solovyev" => "setixela@gmail.com" }
  s.source       = { :git => "https://setixela@bitbucket.org/setixela/setixelautils.git", :tag => "master" }
  s.homepage     = "http://xyzproject.ru"
  s.description  = "Description of SetixelaUtils."
  s.source_files  = "Classes/**/*.*"
  s.platform     = :ios, '9.0'
  s.requires_arc = true
 # s.exclude_files = "Classes/Exclude"

end
